# RLCraft Minecraft Server

dockerized rlcraft server.
early build so far.


ENV VARS:

WHITELIST/OP:
you will need names+UUID.
You will need to set OPSUUID/OPS docker environmental variable as comma delimited string.
Same with WL and WLUUID.

So something like, WLUUID: 123,345,323    & WL: bill,bob,jim
bill = 123
bob = 345
jim = 323

They have to be in the right order, or bad things will happen.



TBD:
automatic backups of world with hourly restart?
proper server startup script, run it with screen so we can attach and detach easily for admin needs.

## 0.4
added more env vars.
also whitelist / ops support. you will need username+uuid.
added documentation for env vars, and for WL/OPs setup/usage.

## 0.3 
added many env vars.

## 0.2
updated to use server build of java.
updated start command.
added this readme

## 0.1
first working build. bare minimum.


