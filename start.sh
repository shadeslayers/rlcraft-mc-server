#!/bin/bash

#echo "DEBUG A"

#Get into our working directory.
cd /opt/minecraft

#Check to make sure they accepted EULA.
if [ "$EULA" = true ]; then
    sed -i 's/false/TRUE/g' eula.txt
fi


#Below are environmental vars that are being placed into appropriate locations. Most of these are lines in server.propertie that alter a server setting.
#If the string is not empty, then we put whatever is in that string into a location.. basically.
#That -n basically checks to make sure the string is non-zero. After all, if you did not provide the env var, we don't want to override the default with a blank.


if [[ -n "$MOTD" ]]; then
    sed -i "/motd\s*=/ c motd=$MOTD" server.properties
fi
if [[ -n "$PORT" ]]; then
    sed -i "/server-port\s*=/ c server-port=$PORT" server.properties
fi
if [[ -n "$SEED" ]]; then
    sed -i "/level-seed\s*=/ c level-seed=$SEED" server.properties
fi
if [[ -n "$HEIGHT" ]]; then
    sed -i "/max-build-height\s*=/ c max-build-height=$HEIGHT" server.properties
fi
if [[ -n "$MAXPLAYERS" ]]; then
    sed -i "/max-players\s*=/ c max-players=$MAXPLAYERS" server.properties
fi
if [[ -n "$ONLINE" ]]; then
    sed -i "/online-mode\s*=/ c online-mode=$ONLINE" server.properties
fi
if [[ -n "$TIMEOUT" ]]; then
    sed -i "/player-idle-timeout\s*=/ c player-idle-timeout=$TIMEOUT" server.properties
fi
if [[ -n "$PVP" ]]; then
    sed -i "/pvp\s*=/ c pvp=$PVP" server.properties
fi
if [[ -n "$VIEWDISTANCE" ]]; then
    sed -i "/view-distance\s*=/ c view-distance=$VIEWDISTANCE" server.properties
fi
if [[ -n "$WHITELIST" ]]; then
    sed -i "/white-list\s*=/ c white-list=$WHITELIST" server.properties
fi

#Env vars for setting ops and whitelist below.
#If the list isn't empty, we want to basically replace the commas with new lines to make a proper list.

if [[ -n "$OPS" ]]; then
  if [[ -n "$OPSUUID" ]]; then
    echo "[" > ops.json

    IFS=',' read -ra names <<< "$OPS"
    IFS=',' read -ra uuids <<< "$OPSUUID"    
    flag=false
    
    for (( c=$((${#names[@]} - 1)); c>=0; c--))
    do
      if $flag; then
        echo "," >> ops.json
      fi
      echo "  {" >> ops.json
      echo "    \"uuid\": \"${uuids[$(($c}}]}\"," >> ops.json
      echo "    \"name\": \"${names[$(($c))]}\"" >> ops.json
      echo -r "  }" >> ops.json
      flag=true
    done
    echo "]" >> ops.json
  fi
fi




if [[ -n "$WL" ]]; then
  if [[ -n "$WLUUID" ]]; then
    echo "[" > whitelist.json

    IFS=',' read -ra names <<< "$WL"
    IFS=',' read -ra uuids <<< "$WLUUID"    
    flag=false
    
    for (( c=$((${#names[@]} - 1)); c>=0; c--))
    do
      if $flag; then
        echo "," >> whitelist.json
      fi
      echo "  {" >> whitelist.json
      echo "    \"uuid\": \"${uuids[$c]}\"," >> whitelist.json
      echo "    \"name\": \"${names[$c]}\"" >> whitelist.json
      echo -e "  }" >> whitelist.json
      flag=true
    done
    echo "]" >> whitelist.json
  fi
fi





#server start command
/opt/jdk1.8.0_231/bin/java -server -Xms2G -Xmx8G -XX:+UseG1GC -jar /opt/minecraft/forge-1.12.2-14.23.5.2847-universal.jar nogui

#echo "DEBUG B"
