FROM java:8

RUN apt-get install -y tar wget unzip tzdata
RUN adduser --disabled-password --home=/opt/minecraft --gecos "minecraft" minecraft


WORKDIR /opt
COPY server-jre-8u231-linux-x64.tar.gz .
RUN tar -zxvf server-jre-8u231-linux-x64.tar.gz
RUN chown -R minecraft:minecraft /opt/

WORKDIR /opt/minecraft
RUN wget https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.12.2-14.23.5.2847/forge-1.12.2-14.23.5.2847-installer.jar
RUN /opt/jdk1.8.0_231/bin/java -jar forge-1.12.2-14.23.5.2847-installer.jar --installServer
RUN /opt/jdk1.8.0_231/bin/java -Xms2G -Xmx8G -jar forge-1.12.2-14.23.5.2847-universal.jar nogui

COPY RLCraft.zip .
RUN unzip RLCraft.zip

WORKDIR /opt/minecraft/mods 
COPY Chunk+Pregenerator+V1.12-2.2.jar .
WORKDIR /opt/minecraft


RUN sed -i 's/false/TRUE/g' eula.txt

COPY server.properties . 
RUN chown -R minecraft:minecraft /opt/minecraft/

ADD start.sh /start 
RUN chmod +x /start

WORKDIR /opt/minecraft
USER minecraft
CMD /start
